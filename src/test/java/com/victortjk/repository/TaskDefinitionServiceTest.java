package com.victortjk.repository;

import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import com.victortjk.service.TaskDefinitionService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskDefinitionServiceTest {

  @Autowired
  private TaskDefinitionRepository taskRepository;
  @Autowired
  private TaskDefinitionMirrorRepository mirrorRepository;
  @Autowired
  private TaskDefinitionService service;

  @Test(expected=DataIntegrityViolationException.class)
  public void testInsertingNullValues() {
    final TaskDefinitionMirror task = new TaskDefinitionMirror(null, null);
    mirrorRepository.save(task);
  }

  @Test
  public void testSyncBetweenTables() {

    // persisting tasks to task_definition table
    final TaskDefinition task1 = new TaskDefinition("1", "A");
    final TaskDefinition task2 = new TaskDefinition("2", "B");
    final TaskDefinition task3 = new TaskDefinition("3", "C");
    List<TaskDefinition> taskList = Arrays.asList(task1, task2, task3);

    taskRepository.saveAll(taskList);

    // propagating to task_definition_mirror table
    service.syncTables();

    //comparing if rows in both tables are the same
    List<TaskDefinitionMirror> mirrorList = mirrorRepository.findAll();
    List<TaskDefinition> propagatedTasks = mirrorList.stream().map(mirror -> TaskDefinition.createFromMirror(mirror)).collect(Collectors.toList());
    propagatedTasks.removeAll(taskList);

    assertTrue(propagatedTasks.isEmpty());

    // testing update, delete and insert in both tables
    TaskDefinitionMirror updateTask = mirrorList.get(2);
    updateTask.setDescription("updated");

    mirrorRepository.save(updateTask);
    taskRepository.deleteById(1l);
    taskRepository.save(new TaskDefinition("XXX", "xxx"));

    service.syncTables();

    mirrorList = mirrorRepository.findAll();
    taskList = taskRepository.findAll();

    propagatedTasks = mirrorList.stream().map(mirror -> TaskDefinition.createFromMirror(mirror)).collect(Collectors.toList());
    propagatedTasks.removeAll(taskList);

    assertTrue(propagatedTasks.isEmpty());
  }
}