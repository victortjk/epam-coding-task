package com.victortjk.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.victortjk.repository.TaskDefinition;
import com.victortjk.repository.TaskDefinitionMirror;
import com.victortjk.repository.TaskDefinitionMirrorRepository;
import com.victortjk.repository.TaskDefinitionRepository;
import com.victortjk.tasks.PropagateToDefinitionTask;
import com.victortjk.tasks.PropagateToMirrorTask;

@Service
public class TaskDefinitionService {

  private final ExecutorService threadPool;
  private final Map<Long, TaskDefinition> tasks;
  private final Map<Long, TaskDefinitionMirror> tasksMirror;

  private final TaskDefinitionRepository taskRepo;
  private final TaskDefinitionMirrorRepository taskMirrorRepo;

  @Autowired
  public TaskDefinitionService(final TaskDefinitionRepository taskRepo, final TaskDefinitionMirrorRepository taskMirrorRepo) {
    this.tasks = new ConcurrentHashMap<>();
    this.tasksMirror = new ConcurrentHashMap<>();
    this.threadPool= Executors.newFixedThreadPool(2); 
    this.taskRepo = taskRepo;
    this.taskMirrorRepo = taskMirrorRepo;
  }

  public void syncTables() {
    try {
      threadPool.submit(new PropagateToMirrorTask(taskRepo, taskMirrorRepo, tasks, tasksMirror)).get();
      threadPool.submit(new PropagateToDefinitionTask(taskRepo, taskMirrorRepo, tasks, tasksMirror)).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e);
    }
  }
}