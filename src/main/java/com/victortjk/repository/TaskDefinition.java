package com.victortjk.repository;

import javax.persistence.Entity;

@Entity
public class TaskDefinition extends AbstractEntity {

  public TaskDefinition () {
    
  }

  public TaskDefinition(final String name, final String description) {
    super(name, description);
  }

  public static TaskDefinition createFromMirror(final TaskDefinitionMirror mirror) {
    final TaskDefinition task= new TaskDefinition(mirror.getName(), mirror.getDescription());
    task.setId(mirror.getId());
    return task;
  }  
}