package com.victortjk.repository;

import javax.persistence.Entity;

@Entity
public class TaskDefinitionMirror extends AbstractEntity {

  public TaskDefinitionMirror() {
    
  }

  public TaskDefinitionMirror(final String name, final String description) {
    super(name, description);
  }

  public static TaskDefinitionMirror createFromTask(final TaskDefinition task) {
    final TaskDefinitionMirror taskDefinitionMirror = new TaskDefinitionMirror(task.getName(), task.getDescription());
    taskDefinitionMirror.setId(task.getId());
    return taskDefinitionMirror;
  }
}