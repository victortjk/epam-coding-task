package com.victortjk.enums;

public enum Action {
  INSERT, UPDATE, DELETE
}