package com.victortjk.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.victortjk.service.TaskDefinitionService;

@Component
public class ScheduledTask {

  private final TaskDefinitionService service;

  @Autowired
  public ScheduledTask(final TaskDefinitionService service) {
    this.service = service;
  }

  @Scheduled(fixedDelay = 5000)
  public void syncTables() {
    service.syncTables();
  }
}