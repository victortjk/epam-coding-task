package com.victortjk.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import com.victortjk.enums.Action;
import com.victortjk.repository.TaskDefinition;
import com.victortjk.repository.TaskDefinitionMirror;
import com.victortjk.repository.TaskDefinitionMirrorRepository;
import com.victortjk.repository.TaskDefinitionRepository;

public class PropagateToMirrorTask extends AbstractTask implements Runnable {

  public PropagateToMirrorTask(final TaskDefinitionRepository taskRepo,
                               final TaskDefinitionMirrorRepository taskMirrorRepo, 
                               final Map<Long, TaskDefinition> tasks, 
                               final Map<Long, TaskDefinitionMirror> tasksMirror) {
    super(taskRepo, taskMirrorRepo, tasks, tasksMirror);
  }

  @Override
  public void run() {
    log.info("starting to sync task definition with mirror");
    final Map<TaskDefinitionMirror, Action> actionsTaskMirror = new TreeMap<>((task1, task2) -> task1.getId().compareTo(task2.getId()));
    final List<TaskDefinition> currentTasksList = taskRepo.findAll();

    boolean wasEmpty = false;

    // populating tasks
    if (this.tasks.isEmpty()) {
      currentTasksList.forEach(current -> {
        tasks.put(current.getId(), current);
        actionsTaskMirror.put(TaskDefinitionMirror.createFromTask(current), Action.INSERT);
      });
      wasEmpty =true;
    }

    if (!wasEmpty) {
      // update and insert rows
      currentTasksList.forEach(task -> {

        final TaskDefinition oldTask = tasks.get(task.getId());
        if (oldTask == null) {
          actionsTaskMirror.put(TaskDefinitionMirror.createFromTask(task), Action.INSERT);
        } else if (!oldTask.equals(task)) {
          //TODO - what if when updating mirror table, it had this row deleted/modified?
          actionsTaskMirror.put(TaskDefinitionMirror.createFromTask(task), Action.UPDATE);
        }

        tasks.put(task.getId(), task);
      });
    }

    // delete rows
    final List<TaskDefinition> taskDiff = new ArrayList<>(tasks.values());
    taskDiff.removeAll(currentTasksList);
    taskDiff.forEach(diff -> {
      actionsTaskMirror.put(TaskDefinitionMirror.createFromTask(diff), Action.DELETE);
      tasks.remove(diff.getId());
    });

    actionsTaskMirror.forEach((task, action) -> {
      switch (action) {
        case INSERT:
          task.setId(null);
        case UPDATE:
          taskMirrorRepo.save(task);
          tasksMirror.put(task.getId(), task);
          break;
        case DELETE:
          taskMirrorRepo.delete(task);
          tasksMirror.remove(task.getId());
      }
    });
    log.info("table mirror synced at {}", dateFormat.format(new Date()));
  }
}