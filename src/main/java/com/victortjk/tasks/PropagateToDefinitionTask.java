package com.victortjk.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import com.victortjk.enums.Action;
import com.victortjk.repository.TaskDefinition;
import com.victortjk.repository.TaskDefinitionMirror;
import com.victortjk.repository.TaskDefinitionMirrorRepository;
import com.victortjk.repository.TaskDefinitionRepository;

public class PropagateToDefinitionTask extends AbstractTask implements Runnable {

  public PropagateToDefinitionTask(final TaskDefinitionRepository taskRepo,
                                   final TaskDefinitionMirrorRepository taskMirrorRepo, 
                                   final Map<Long, TaskDefinition> tasks, 
                                   final Map<Long, TaskDefinitionMirror> tasksMirror) {
    super(taskRepo, taskMirrorRepo, tasks, tasksMirror);
  }

  @Override
  public void run() {
    log.info("starting to sync mirror with task");
    final Map<TaskDefinition, Action> actionsTask = new TreeMap<>((task1, task2) -> task1.getId().compareTo(task2.getId()));
    final List<TaskDefinitionMirror> currentTasksMirrorList = taskMirrorRepo.findAll();
    boolean wasEmpty = false;

    // populating tasks
    if (this.tasksMirror.isEmpty()) {
      currentTasksMirrorList.forEach(current -> {
        tasksMirror.put(current.getId(), current);
        actionsTask.put(TaskDefinition.createFromMirror(current), Action.INSERT);
      });
      wasEmpty =true;
    }

    if (!wasEmpty) {
      // update and insert rows
      currentTasksMirrorList.forEach(task -> {

        final TaskDefinitionMirror oldTask = tasksMirror.get(task.getId());
        if (oldTask == null) {
          actionsTask.put(TaskDefinition.createFromMirror(task), Action.INSERT);
        } else if (!oldTask.equals(task)) {
          //TODO - what if when updating mirror table, it had this row deleted/modified?
          actionsTask.put(TaskDefinition.createFromMirror(task), Action.UPDATE);
        }

        tasksMirror.put(task.getId(), task);
      });
    }

    // delete rows
    final List<TaskDefinitionMirror> taskMirrorDiff = new ArrayList<>(tasksMirror.values());
    taskMirrorDiff.removeAll(currentTasksMirrorList);
    taskMirrorDiff.forEach(diff -> {
      actionsTask.put(TaskDefinition.createFromMirror(diff), Action.DELETE);
      tasksMirror.remove(diff.getId());
    });

    actionsTask.forEach((task, action) -> {
      switch (action) {
        case INSERT:
          task.setId(null);
        case UPDATE:
          taskRepo.save(task);
          tasks.put(task.getId(), task);
          break;
        case DELETE:
          taskRepo.delete(task);
          tasks.remove(task.getId());
      }
    });
    log.info("table task synced at {}", dateFormat.format(new Date()));
  }
}