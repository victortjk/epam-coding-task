package com.victortjk.tasks;

import java.text.SimpleDateFormat;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.victortjk.repository.TaskDefinition;
import com.victortjk.repository.TaskDefinitionMirror;
import com.victortjk.repository.TaskDefinitionMirrorRepository;
import com.victortjk.repository.TaskDefinitionRepository;

public abstract class AbstractTask {
  protected final Map<Long, TaskDefinition> tasks;
  protected final Map<Long, TaskDefinitionMirror> tasksMirror;
  protected final TaskDefinitionRepository taskRepo;
  protected final TaskDefinitionMirrorRepository taskMirrorRepo;

  protected static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);
  protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

  public AbstractTask(final TaskDefinitionRepository taskRepo,
                               final TaskDefinitionMirrorRepository taskMirrorRepo, 
                               final Map<Long, TaskDefinition> tasks, 
                               final Map<Long, TaskDefinitionMirror> tasksMirror) {
    this.taskRepo = taskRepo;
    this.tasksMirror = tasksMirror;
    this.tasks = tasks;
    this.taskMirrorRepo = taskMirrorRepo;
  }
}