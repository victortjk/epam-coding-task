CREATE TABLE TASK_DEFINITION (
	id BIGINT auto_increment primary key,
	name varchar(255) not null,
	description varchar(255) not null
);

CREATE TABLE TASK_DEFINITION_MIRROR (
	id BIGINT auto_increment primary key,
	name varchar(255) not null,
	description varchar(255) not null
);